{
  config,
  pkgs,
  ...
}: {
  programs.fish = {
    enable = true;
    shellInit = ''
      set -u fish_greeting ""
      fish_vi_key_bindings
    '';
  };
  home.packages = with pkgs; [
    fishPlugins.puffer
    fishPlugins.fzf
    fishPlugins.colored-man-pages
    fishPlugins.z
  ];
}
