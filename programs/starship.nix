{
  programs.starship = {
    enable = true;
    enableBashIntegration = true;
    enableFishIntegration = true;
    settings = {
      add_newline = false;
      scan_timeout = 50;
      character = {
        success_symbol = "➜";
        error_symbol = "➜";
      };
    };
  };
}
