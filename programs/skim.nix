{
  pkgs,
  config,
  ...
}: {
  programs.skim = {
    enable = true;
    enableFishIntegration = true;
    enableBashIntegration = true;
    defaultCommand = "fd --type f";
  };
}
