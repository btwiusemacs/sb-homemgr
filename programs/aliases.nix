{
  home.shellAliases = {
    cl = "clear";
    cll = "clear && ls -l";
    cls = "clear && ls -lah";
    gogh = "bash -c  '$(wget -qO- https://git.io/vQgMr)'";
    hc = "herbstclient";
    hms = "home-manager switch --flake .";
    lg = "lazygit";
    jb = "just nixos-boot";
    jh = "just home-switch";
    jn = "just nixos-switch";
    ju = "just update-nix";
    my = "z mynix";
    v = "nvim";
    cat = "bat";
    catp = "bat -p";
    yta = "yt-dlp -f 'bestaudio[ext=m4a]','bestaudio[ext=webm]' -x $argv";
  };
}
