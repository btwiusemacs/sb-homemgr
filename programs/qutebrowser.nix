{
  config,
  pkgs,
  lib,
  ...
}: {
  home.packages = [ pkgs.qutebrowser ];
  programs.qutebrowser = {
    enable = true;
    settings = {
      auto_save.session = true;
      colors.webpage = {preferred_color_scheme = "dark";};
    };
    extraConfig = ''
      config.load_autoconfig(False)
      config.set('content.javascript.enabled', True, 'file://*')
      config.set('content.javascript.enabled', True, 'chrome://*/*')
      config.set('content.javascript.enabled', True, 'qute://*/*')
      config.set('tabs.padding', { 'top': 5, 'bottom': 5, 'left': 5, 'right': 5 })
      config.set('fonts.default_family', 'Cantarell')
      config.set('fonts.default_size', '12pt')
      config.set('statusbar.padding', { "bottom":2, "left":2, "top":2, "right":2})
      config.set('zoom.default', '80%')
    '';
    quickmarks = {
      hm = "https://nix-community.github.io/home-manager/options.html";
      bw = "https://vault.bitwarden.com/#/vault";
      nixIntro = "https://nixos-and-flakes.thiscute.world/introduction/";
      perplexity = "https://www.perplexity.ai/";
      stylix = "https://danth.github.io/stylix/index.html";
    };
    searchEngines = {
      DEFAULT = "https://duckduckgo.com/>q={}";
      aw = "https://wiki.archlinux.org/title/{}";
      gh = "https://github.com/search?q={}&type=repositories";
      nso = "https://search.nixos.org/options?channel=unstable&from=0&size=50&sort=relevance&type=packages&query={}"; # query unstable pkgs.
      nsp = "https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query={}"; # query unstable pkgs.
      nw = "https://nixos.wiki/index.php?search={}";
      yt = "https://www.youtube.com/results?search_query={}";
    };
  };
}
